﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainController : MonoBehaviour
{
	float semitoneConstant = 1.05946309436f;
	float player1Pos;


	//find the position of the a semitone interval
	float[] semitoneRatios = new float[12]{
		0.056f, //F
		0.109f, //F#
		0.159f, //G
		0.206f, //G#
		0.251f, //A
		0.293f, //A#
		0.333f, //B
		0.370f, //C
		0.405f, //C#
		0.439f, //D
		0.470f, //D#
		0.5f  	//E
	};

	public float findSemitoneLengthUp(float currentPos, int semitone)
	{

		return -1;
	}
	public float findSemitoneLengthDown(float currentPos, int semitone)
	{
		return -1;
	}

	public float findSemitoneFrequency(int semitone){
		return Mathf.Pow(semitoneConstant, semitone);
	}
	public float getSemitoneFrequencyFromLength(float len){
		return 1/len;
	}

	public float getClosestSemitoneFromLength(float length){

		return -1;
	}
	public float getFrequencyFromLength(float length){
		return 1/length;
	}

	public int findClosestSemitone(float freq){
		float curFreq = 164.81f;
		int curSemitone = 0;
		float prevDif = Mathf.Abs(curFreq-freq);
		// Debug.Log("prev "+prevDif);
		
		curSemitone = 0;
		curFreq = findSemitoneFrequency(1)*curFreq;
		float curDif = Mathf.Abs(curFreq-freq);
		// Debug.Log("cur "+curDif);
		while(curDif<prevDif){
			prevDif = curDif;

			curFreq *= findSemitoneFrequency(1);
			curDif = Mathf.Abs(curFreq-freq);

			curSemitone++;

			if(curSemitone>100){ Debug.LogError("shit"); break;}
		}
		return curSemitone;
	}

	public int getSemitoneFromLength(float len){
		float freq = (1/len)*164.81f;
		return findClosestSemitone(freq);
	}

	void Start()
	{	float auxlen = (1-1f/findSemitoneFrequency(6));
		// Debug.Log("TEST> "+auxlen);
		
		// Debug.Log("E> "+(1/auxlen)*164.81f);

		// Debug.Log("Closest semitone of: "+554.37f+" is: "+findClosestSemitone(554.37f));


		// //  pick a fret  -> left and right side
		// // 
		// Debug.Log(getSemitoneFromLength(1));
		// Debug.Log(getSemitoneFromLength(0.5f));
		// Debug.Log(getSemitoneFromLength(0.33f));
		
	}
}
