﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelController : MonoBehaviour
{
	public Song currentSong;

	float currentTime;
	NoteType currentExpectedNote;

	[SerializeField] Transform noteParents;

	[SerializeField] Transform noteObjectPrefab;
	List<Transform> noteObjects = new List<Transform>();

	void Start()
	{
		currentTime = 0;
		currentExpectedNote = NoteType.None;

		currentSong = new Song();

		foreach((NoteType,float) pair in currentSong.songEvents)
		{
			var n = Instantiate(noteObjectPrefab);
			n.SetParent(noteParents);
			n.GetComponent<Image>().color = Color.red;
			n.localPosition = new Vector3(100 * pair.Item2, 0);
			noteObjects.Add(n);
		}
	}

	void Update()
	{
		currentTime += Time.deltaTime;

		noteParents.localPosition = new Vector3(noteParents.localPosition.x - 100*Time.deltaTime, noteParents.localPosition.y);

		foreach((NoteType,float) pair in currentSong.songEvents)
		{
			if (currentTime < pair.Item2)
			{
				currentExpectedNote = pair.Item1;
				Debug.Log("CurrentExpectedNote: " + currentExpectedNote);
				break;
			}
		}

	}
}
